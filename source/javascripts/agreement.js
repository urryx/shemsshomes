(() => {
  const params = location.search.slice(1).split('&')
  .map(part => part.split('=').map(decodeURIComponent))
  .reduce((obj, pair) => ({ ...obj, [pair[0]]: pair[1] }), {})

  const dict = {
    'silver-7-0,ext-kings-canyon,rh,int-light': {
      name: 'Silver 7.0 — Kings Canyon Exterior, Right-Handed, Light Interior',
      url: 'https://buy.stripe.com/fZeeVD1Qtafn7MA5kP',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-kings-canyon,rh,int-dark': {
      name: 'Silver 7.0 — Kings Canyon Exterior, Right-Handed, Dark Interior',
      url: 'https://buy.stripe.com/aEU8xfbr3afneaY00w',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-kings-canyon,lh,int-light': {
      name: 'Silver 7.0 — Kings Canyon Exterior, Left-Handed, Light Interior',
      url: 'https://buy.stripe.com/14k3cV66JgDLd6U3cK',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-kings-canyon,lh,int-dark': {
      name: 'Silver 7.0 — Kings Canyon Exterior, Left-Handed, Dark Interior',
      url: 'https://buy.stripe.com/14k4gZdzbcnv0k89B7',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-crossroads,rh,int-light': {
      name: 'Silver 7.0 — Crossroads Exterior, Right-Handed, Light Interior',
      url: 'https://buy.stripe.com/28o5l30Mpdrzff214D',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-crossroads,rh,int-dark': {
      name: 'Silver 7.0 — Crossroads Exterior, Right-Handed, Dark Interior',
      url: 'https://buy.stripe.com/9AQ28R7aNevDeaY7t2',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-crossroads,lh,int-light': {
      name: 'Silver 7.0 — Crossroads Exterior, Left-Handed, Light Interior',
      url: 'https://buy.stripe.com/aEUeVD52Fbjrc2QdRw',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-crossroads,lh,int-dark': {
      name: 'Silver 7.0 — Crossroads Exterior, Left-Handed, Dark Interior',
      url: 'https://buy.stripe.com/6oE14N8eRcnv9UIfZz',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-ash-tree,rh,int-light': {
      name: 'Silver 7.0 — Ash Tree Exterior, Right-Handed, Light Interior',
      url: 'https://buy.stripe.com/28o7tb52FevD8QEeVy',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-ash-tree,rh,int-dark': {
      name: 'Silver 7.0 — Ash Tree Exterior, Right-Handed, Dark Interior',
      url: 'https://buy.stripe.com/fZe5l33YB9bj2sg3cO',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-ash-tree,lh,int-light': {
      name: 'Silver 7.0 — Ash Tree Exterior, Left-Handed, Light Interior',
      url: 'https://buy.stripe.com/dR6aFndzb5Z7aYMcNr',
      bgClass: 'silver-home-bg',
    },
    'silver-7-0,ext-ash-tree,lh,int-dark': {
      name: 'Silver 7.0 — Ash Tree Exterior, Left-Handed, Dark Interior',
      url: 'https://buy.stripe.com/8wM5l3gLn73b8QEdRt',
      bgClass: 'silver-home-bg',
    },
  }

  const key = [
    params.model,
   'ext-' + params.exterior,
    params.layout,
    params.interior,
  ].join()

  const model = dict[key]

  if (model) {
    document.getElementById('agreement').textContent = model.name
    document.getElementById('agreement-form').action = model.url
    document.getElementById('agreement-form').classList.remove('d-none')
    document.getElementById('agreement-check').addEventListener('change', e => {
      document.getElementById('agreement-button').disabled = !e.target.checked
    })
    document.getElementById('home').className = model.bgClass
  } else {
    document.getElementById('agreement').textContent =
      'No model found with given configuration'
    console.log(key)
  }

})()