;var capitalize = function capitalize (str) {
    return str[0].toUpperCase().concat(str.slice(1))
};

;document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('.configurator-root').forEach(function (root) {
        var header = root.querySelector('[data-model]')

        var preload = document.createElement('div')
        preload.style.width = '10px'
        preload.style.height = '10px'
        preload.style.position = 'absolute'
        preload.style.left = '-100px'
        preload.style.top = '-100px'
        preload.style.overflow = 'hidden'
        document.body.appendChild(preload)

        var setSpecs = function setSpecs () {
            root.querySelectorAll('.model-specs').forEach(function (specs) {
                var exterior = root.querySelector('.config-btn[name="exterior"]:checked').value
                var layout = root.querySelector('.config-btn[name="layout"]:checked').value
                var interior = root.querySelector('.config-btn[name="interior"]:checked').value
                specs.textContent =
                exterior.split('-').map(capitalize).join(' ')
                .concat(' Exterior, ')
                .concat({ lh: 'Left-Handed', rh: 'Right-Handed' }[layout])
                .concat(', ')
                .concat(capitalize(interior.split('-')[1]))
                .concat(' Interior')
            })
        }

        root.querySelectorAll('.config-btn').forEach(function (button) {
            var selector = '.config-btn[data-side="' + button.dataset.side + '"]'
            var peers = root.querySelectorAll(selector)
            var others = Array.prototype.filter.call(peers, function (peer) {
                return peer.name != button.name
            })

            if (others.length) {
                others.forEach(function (other) {
                    var n = document.createElement('div')
                    n.className = [ header.dataset.model, button.value, other.value ].join('-')
                    preload.appendChild(n)
                })
            } else {
                var n = document.createElement('div')
                n.className = [ header.dataset.model, button.value ].join('-')
                preload.appendChild(n)
            }
        })

        root.addEventListener('click', function (e) {
            var target = e.target.closest('.config-btn')
            if (target) {
                var selector = '.config-btn[data-side="' + target.dataset.side + '"]:checked'
                var buttons = root.querySelectorAll(selector)
                var values = Array.prototype.map.call(buttons, function (button) {
                    return button.value
                })
                header.className = [ header.dataset.model ].concat(values).join('-')
                setSpecs()
            }
        })

        setSpecs()
    })
});
